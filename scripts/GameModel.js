/**
 * Created by Nikhil on 11/30/2015.
 */
/// <reference path='Squirrel.ts'/>
/// <reference path='Ball.ts'/>
/// <reference path='Nut.ts'/>
/// <reference path='Level.ts'/>
var TheGame;
(function (TheGame) {
    var GameModel = (function () {
        function GameModel() {
            this._squirrels = [];
            this._nuts = [];
            this._timeSinceLastSquirrel = 0;
            this.SoundsToPlay = [];
            this._defaultSling = new TheGame.Vector2N(622, 504);
            this.SlingPosition = this._defaultSling;
            this.BallIsFired = false;
            this.FiredBall = null;
            this.CurrentLevel = new TheGame.Level();
            this.CurrentLevel.InitializeLevel(1);
        }
        GameModel.prototype.DefaultPosition = function () {
            return this._defaultSling.Clone();
        };
        GameModel.prototype.MouseDown = function (position) {
            if (this.BallIsFired == false && this._defaultSling.DistanceFrom(position) < 150) {
                this.SlingPosition = position;
                return true;
            }
            return false;
        };
        GameModel.prototype.MouseMove = function (position) {
            if (this._defaultSling.DistanceFrom(position) < 150) {
                this.SlingPosition = position;
            }
            else {
                var diff = position.Diff(this._defaultSling);
                var size = diff.Size();
                var scale = 150 / size;
                var posVector = diff.Scale(scale);
                this.SlingPosition = this._defaultSling.Add(posVector);
            }
        };
        GameModel.prototype.MouseUp = function (position) {
            this.BallIsFired = true;
            this.FiredBall = new TheGame.Ball();
            this.FiredBall.CurrentPosition = this.SlingPosition;
            var velocity = this._defaultSling.Diff(this.SlingPosition);
            this.FiredBall.Velocity = velocity.Scale(10);
            this.SlingPosition = this._defaultSling;
            this.SoundsToPlay.push('shoot');
            this.CurrentLevel.UseApple();
        };
        GameModel.prototype.ResetLevel = function () {
            this._squirrels = [];
            this._nuts = [];
            this._timeSinceLastSquirrel = 0;
        };
        GameModel.prototype.Step = function (delta) {
            if (this.CurrentLevel.GetCurrentNutCount() >= this.CurrentLevel.TargetNuts) {
                var lvlNumber = this.CurrentLevel.LevelNumber;
                ga('send', 'event', 'Game', 'LevelComplete', 'Level ' + lvlNumber, lvlNumber);
                this.CurrentLevel.InitializeLevel(lvlNumber + 1);
                this.ResetLevel();
            }
            else if (this.CurrentLevel.GetApplesLeft() == 0 && this.BallIsFired == false) {
                this.CurrentLevel.InitializeLevel(1);
                this.ResetLevel();
            }
            var squirrels = this._squirrels;
            var length = squirrels.length;
            var toDelete = [];
            var current;
            for (var i = 0; i < length; ++i) {
                squirrels[i].Step(delta);
                current = squirrels[i];
                if (current.CurrentPosition.X < -250
                    || current.CurrentPosition.X > 1500
                    || current.CurrentPosition.Y < -250
                    || current.CurrentPosition.Y > 1000) {
                    toDelete.push(current);
                }
            }
            if (this.BallIsFired) {
                current = this.FiredBall;
                current.Step(delta);
                if (current.CurrentPosition.X < -250
                    || current.CurrentPosition.X > 1500
                    || current.CurrentPosition.Y < -250
                    || current.CurrentPosition.Y > 1000) {
                    this.BallIsFired = false;
                    this.FiredBall = null;
                }
            }
            this._timeSinceLastSquirrel += delta;
            if (this._timeSinceLastSquirrel > 5.0) {
                var levelNumber = 1;
                if (this.CurrentLevel != null) {
                    levelNumber = this.CurrentLevel.LevelNumber;
                }
                var maxNumberOfSquirrels = 1;
                switch (levelNumber) {
                    case 1:
                    case 2:
                        maxNumberOfSquirrels = 1;
                        break;
                    default:
                        maxNumberOfSquirrels = 3;
                }
                var numSquirrels = Math.ceil(Math.random() * maxNumberOfSquirrels);
                var startPositions = this.GetStartPositions(numSquirrels);
                for (var k = 0; k < numSquirrels; ++k) {
                    var rand = Math.floor((Math.random() * 2));
                    var startPosition = startPositions[k];
                    var startHeight = Math.floor(Math.random() * 3) * 50;
                    startPosition.Y = 180 + startHeight;
                    var squirrel = new TheGame.Squirrel();
                    squirrel.Poison = false;
                    if (this.CurrentLevel != null) {
                        var chance = this.CurrentLevel.Chance;
                        var r = Math.random();
                        if (r < chance) {
                            squirrel.Poison = true;
                        }
                    }
                    squirrel.CurrentPosition = startPosition;
                    var speedMultiplier = Math.random() + 0.5;
                    if (startPosition.X == 115) {
                        squirrel.Direction = 1;
                        squirrel.ApplyImpulse(new TheGame.Vector2N(300 * speedMultiplier, 0));
                    }
                    else {
                        squirrel.Direction = -1;
                        squirrel.ApplyImpulse(new TheGame.Vector2N(-300 * speedMultiplier, 0));
                    }
                    this._squirrels.push(squirrel);
                }
                this._timeSinceLastSquirrel = 0;
            }
            this.CheckCollisions();
            for (var j = 0; j < toDelete.length; ++j) {
                for (i = 0; i < this._squirrels.length; ++i) {
                    if (toDelete[j] == this._squirrels[i]) {
                        this._squirrels.splice(i, 1);
                        break;
                    }
                }
            }
        };
        GameModel.prototype.GetStartPositions = function (numberOfPositions) {
            //Generate all possible start positions
            var possibleXs = [115, 1244 - 115];
            var possibleYs = [180, 230, 280];
            var toReturn = [];
            for (var x = 0; x < possibleXs.length; ++x) {
                for (var y = 0; y < possibleYs.length; ++y) {
                    var temp = new TheGame.Vector2N(possibleXs[x], possibleYs[y]);
                    toReturn.push(temp);
                }
            }
            toReturn = this.Shuffle(toReturn);
            return toReturn.slice(0, numberOfPositions);
        };
        GameModel.prototype.Shuffle = function (array) {
            var currentIndex = array.length, temporaryValue, randomIndex;
            // While there remain elements to shuffle...
            while (0 !== currentIndex) {
                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
            return array;
        };
        GameModel.prototype.CheckCollisions = function () {
            if (!this.BallIsFired) {
                return;
            }
            var ball = this.FiredBall;
            var squirrels = this._squirrels;
            var length = squirrels.length;
            for (var i = 0; i < length; ++i) {
                var squirrel = squirrels[i];
                if (squirrel.Velocity.X == 0) {
                    continue;
                }
                var distance = ball.CurrentPosition.DistanceFrom(squirrel.CurrentPosition);
                if (distance > ball.Radius + squirrel.Radius) {
                    continue;
                }
                if (!this.Intersect(squirrel, ball)) {
                    continue;
                }
                console.log('hit');
                squirrel.Velocity = new TheGame.Vector2N();
                squirrel.HasGravity = true;
                if (squirrel.Poison == true) {
                    this.CurrentLevel.GotBadNut();
                }
                else {
                    this.CurrentLevel.GotGoodNut();
                }
                this.SoundsToPlay.push('hit');
            }
        };
        GameModel.prototype.Intersect = function (box1, box2) {
            var min1 = new TheGame.Vector2N(box1.CurrentPosition.X - box1.Width / 2, box1.CurrentPosition.Y - box1.Height / 2);
            var max1 = new TheGame.Vector2N(box1.CurrentPosition.X + box1.Width / 2, box1.CurrentPosition.Y + box1.Height / 2);
            var min2 = new TheGame.Vector2N(box2.CurrentPosition.X - box2.Width / 2, box2.CurrentPosition.Y - box2.Height / 2);
            var max2 = new TheGame.Vector2N(box2.CurrentPosition.X + box2.Width / 2, box2.CurrentPosition.Y + box2.Height / 2);
            if (max1.X < min2.X) {
                return false;
            }
            if (min1.X > max2.X) {
                return false;
            }
            if (max1.Y < min2.Y) {
                return false;
            }
            if (min1.Y > max2.Y) {
                return false;
            }
            return true;
        };
        GameModel.prototype.GetSquirrels = function () {
            return this._squirrels;
        };
        return GameModel;
    })();
    TheGame.GameModel = GameModel;
})(TheGame || (TheGame = {}));
//# sourceMappingURL=GameModel.js.map