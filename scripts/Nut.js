/// <reference path='GameObject.ts'/>
/**
 * Created by Nick on 12/15/2015.
 */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TheGame;
(function (TheGame) {
    var Nut = (function (_super) {
        __extends(Nut, _super);
        function Nut() {
            _super.apply(this, arguments);
            this.Width = 20;
            this.Height = 20;
        }
        return Nut;
    })(TheGame.GameObject);
    TheGame.Nut = Nut;
})(TheGame || (TheGame = {}));
//# sourceMappingURL=Nut.js.map