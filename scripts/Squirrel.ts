/// <reference path='GameObject.ts'/>
/**
 * Created by Nikhil on 11/30/2015.
 */

module TheGame
{
    export class Squirrel extends GameObject
    {
        public Direction: Number;
        public Width = 50;
        public Height = 31;
        public Radius = Math.sqrt(this.Width * this.Width + this.Height * this.Height);
        public Poison: boolean;
    }
}
