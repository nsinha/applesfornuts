/**
 * Created by Nikhil on 12/2/2015.
 */
/// <reference path='GameObject.ts'/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TheGame;
(function (TheGame) {
    var Ball = (function (_super) {
        __extends(Ball, _super);
        function Ball() {
            _super.call(this);
            this.Width = 25;
            this.Height = 25;
            this.Radius = Math.sqrt(this.Width * this.Width + this.Height * this.Height);
            this.HasGravity = true;
        }
        return Ball;
    })(TheGame.GameObject);
    TheGame.Ball = Ball;
})(TheGame || (TheGame = {}));
//# sourceMappingURL=Ball.js.map