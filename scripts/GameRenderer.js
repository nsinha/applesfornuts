/**
 * Created by Nikhil on 11/30/2015.
 */
/// <reference path='GameModel.ts'/>
var TheGame;
(function (TheGame) {
    var GameRenderer = (function () {
        function GameRenderer(canvas, ctx, model) {
            this._images = {};
            this._canvas = canvas;
            this._ctx = ctx;
            this._model = model;
            var tree = new Image();
            tree.src = 'images/ShittyTree.png';
            this._images['tree'] = tree;
            var squirrel = new Image();
            squirrel.src = 'images/ShittySquirrel.png';
            this._images['squirrel'] = squirrel;
            var poisonSquirrel = new Image();
            poisonSquirrel.src = 'images/PoisonSquirrel.png';
            this._images['poisonSquirrel'] = poisonSquirrel;
            var slingshot = new Image();
            slingshot.src = 'images/Slingshot.png';
            this._images['slingshot'] = slingshot;
            var sling = new Image();
            sling.src = 'images/Sling.png';
            this._images['sling'] = sling;
            var apple = new Image();
            apple.src = 'images/Apple.png';
            this._images['apple'] = apple;
        }
        GameRenderer.prototype.Draw = function () {
            var ctx = this._ctx;
            var canvas = this._canvas;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = "LightSkyBlue";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(this._images['tree'], 10, 150);
            ctx.drawImage(this._images['tree'], 1024, 150);
            ctx.drawImage(this._images['slingshot'], 572, 501);
            var slingPos = this._model.SlingPosition;
            if (this._model.BallIsFired) {
                slingPos = this._model.FiredBall.CurrentPosition;
            }
            var leftFixture = new TheGame.Vector2N(572, 501);
            var rightFixture = new TheGame.Vector2N(672, 501);
            var appleSprite = this._images['apple'];
            var appleHalfWidth = appleSprite.width / 2;
            var appleHalfHeight = appleSprite.height / 2;
            ctx.save();
            ctx.translate(slingPos.X, slingPos.Y);
            ctx.drawImage(appleSprite, -appleHalfWidth, -appleHalfHeight);
            ctx.restore();
            var slingSprite = this._images['sling'];
            ctx.save();
            ctx.translate(leftFixture.X, leftFixture.Y);
            var center = leftFixture.Average(rightFixture);
            var translation = slingPos.Diff(new TheGame.Vector2N(leftFixture.X, leftFixture.Y + 3));
            var hypotenuse = Math.min(translation.Size(), 150);
            var cos = translation.X / hypotenuse;
            var angleC = Math.acos(cos);
            var sin = translation.Y / hypotenuse;
            var angleS = Math.asin(sin);
            var angleToUse;
            if (translation.Y >= 0) {
                angleToUse = angleC;
            }
            else if (translation.X > 0) {
                angleToUse = angleS;
            }
            else {
                angleToUse = Math.PI * 2 - angleC;
            }
            if (!this._model.BallIsFired) {
                ctx.rotate(angleToUse);
                ctx.scale(hypotenuse / 50, 1.0);
            }
            ctx.drawImage(slingSprite, 0, 0);
            ctx.restore();
            ctx.save();
            ctx.translate(rightFixture.X, rightFixture.Y);
            translation = slingPos.Diff((new TheGame.Vector2N(rightFixture.X, rightFixture.Y + 3)));
            hypotenuse = Math.min(translation.Size(), 150);
            cos = translation.X / hypotenuse;
            angleC = Math.acos(cos);
            sin = translation.Y / hypotenuse;
            angleS = Math.asin(sin);
            angleToUse;
            if (translation.Y >= 0) {
                angleToUse = angleC;
            }
            else if (translation.X > 0) {
                angleToUse = angleS;
            }
            else {
                angleToUse = Math.PI * 2 - angleC;
            }
            if (!this._model.BallIsFired) {
                ctx.rotate(Math.PI + angleToUse);
                ctx.scale(hypotenuse / 50, 1.0);
            }
            ctx.scale(-1, 1);
            ctx.drawImage(slingSprite, 0, 0);
            ctx.restore();
            var squirrels = this._model.GetSquirrels();
            var length = squirrels.length;
            var sqSprite = this._images['squirrel'];
            var poisonSqSprite = this._images['poisonSquirrel'];
            var sqWidth = sqSprite.width;
            var xOffset = sqWidth / 2;
            var sqHeight = sqSprite.height;
            var yOffset = sqHeight / 2;
            for (var i = 0; i < length; ++i) {
                var squirrel = squirrels[i];
                ctx.save();
                ctx.translate(squirrel.CurrentPosition.X, squirrel.CurrentPosition.Y);
                if (squirrel.Direction == -1) {
                    ctx.scale(-1, 1);
                }
                var squirrelSprite = sqSprite;
                if (squirrel.Poison == true) {
                    squirrelSprite = poisonSqSprite;
                }
                ctx.drawImage(squirrelSprite, -xOffset, -yOffset);
                ctx.restore();
            }
            this.DrawHud(ctx, this._model);
        };
        GameRenderer.prototype.DrawHud = function (ctx, gameModel) {
            ctx.fillStyle = "black";
            ctx.font = "20px Arial";
            var lvl = gameModel.CurrentLevel;
            var toDisplay = "Level: " + lvl.LevelNumber;
            ctx.fillText(toDisplay, 10, 25);
            var nuts = lvl.GetCurrentNutCount();
            var target = lvl.TargetNuts;
            toDisplay = "Nuts: " + nuts + "/" + target;
            ctx.fillText(toDisplay, 10, 50);
            toDisplay = "Apples Left: " + lvl.GetApplesLeft();
            ctx.fillText(toDisplay, 10, 75);
        };
        return GameRenderer;
    })();
    TheGame.GameRenderer = GameRenderer;
})(TheGame || (TheGame = {}));
//# sourceMappingURL=GameRenderer.js.map