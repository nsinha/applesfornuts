/**
 * Created by Nikhil on 11/30/2015.
 */

/// <reference path='GameRenderer.ts'/>
/// <reference path='GameModel.ts'/>
/// <reference path='SoundManager.ts'/>

module TheGame{
    class GameRunner{
        private _renderer: GameRenderer;
        private _model: GameModel;
        private _sounds: SoundManager;
        constructor(canvasId: string){

            var canvas = <HTMLCanvasElement>document.getElementById(canvasId);
            var that = this;
            var dragMove = function(event) {
                var pos = getCursorPosition(event, canvas);
                that._model.MouseMove(new Vector2N(pos.x, pos.y));
            };

            var dragEnd = function (event) {
                var pos = getCursorPosition(event, canvas);
                that._model.MouseUp(new Vector2N(pos.x, pos.y));

                canvas.removeEventListener("mousemove", dragMove, true);
                canvas.removeEventListener("mouseup", dragEnd, true);
            };


            canvas.addEventListener('mousedown', function(event){


                var pos = getCursorPosition(event, canvas);
                if(that._model.MouseDown(new Vector2N(pos.x, pos.y)))
                {
                    canvas.addEventListener("mousemove", dragMove, true);
                    canvas.addEventListener("mouseup", dragEnd, true);
                }
            });
            var ctx = canvas.getContext('2d');
            this._model = new GameModel();
            this._renderer = new GameRenderer(canvas, ctx, this._model);
            this._sounds = new SoundManager();
        }

        Step(){
            this._model.Step(0.017);
        }

        Draw(){
            this._renderer.Draw();
            var sounds = this._model.SoundsToPlay;
            for(var i = 0; i < sounds.length; ++i){
                this._sounds.Play(sounds[i]);
            }
            this._model.SoundsToPlay = [];
        }

        Setup(){

        }
    }

    var runner: GameRunner = new GameRunner('gameCanvas');
    runner.Setup();
    setInterval(function(){
        runner.Draw();
        runner.Step();
    }, 17)


}

function getCursorPosition(e, canvas) {
    var x;
    var y;
    if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
    }
    else {
        x = e.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    return { x: x, y: y };
}