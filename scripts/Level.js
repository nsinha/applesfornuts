/**
 * Created by Nick on 12/15/2015.
 */
var TheGame;
(function (TheGame) {
    var Level = (function () {
        function Level() {
        }
        Level.prototype.InitializeLevel = function (levelNumber) {
            this.LevelNumber = levelNumber;
            var chance = (levelNumber - 1);
            this.Chance = Math.min(chance / 25, .8);
            this.TargetNuts = 5;
            this._apples = Math.max(21 - levelNumber, 5);
            this._currentNuts = 0;
        };
        Level.prototype.UseApple = function () {
            --this._apples;
            if (this._apples == 0) {
                return false;
            }
            return true;
        };
        Level.prototype.GetApplesLeft = function () {
            return this._apples;
        };
        Level.prototype.GotGoodNut = function () {
            ++this._currentNuts;
        };
        Level.prototype.GotBadNut = function () {
            --this._currentNuts;
            if (this._currentNuts < 0) {
                this._currentNuts = 0;
            }
        };
        Level.prototype.GetCurrentNutCount = function () {
            return this._currentNuts;
        };
        return Level;
    })();
    TheGame.Level = Level;
})(TheGame || (TheGame = {}));
//# sourceMappingURL=Level.js.map