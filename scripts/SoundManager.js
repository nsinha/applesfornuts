/**
 * Created by Nikhil on 12/3/2015.
 */
var TheGame;
(function (TheGame) {
    var SoundManager = (function () {
        function SoundManager() {
            this._shoot = new Audio('sounds/Laser_Shoot19.wav');
            this._hit = new Audio('sounds/Hit_Hurt16.wav');
        }
        SoundManager.prototype.Play = function (name) {
            switch (name) {
                case 'shoot':
                    this._shoot.play();
                    break;
                case 'hit':
                    this._hit.play();
                    break;
            }
        };
        return SoundManager;
    })();
    TheGame.SoundManager = SoundManager;
})(TheGame || (TheGame = {}));
//# sourceMappingURL=SoundManager.js.map