/**
 * Created by Nikhil on 12/2/2015.
 */
/// <reference path='GameObject.ts'/>

module TheGame
{
    export class Ball extends GameObject
    {
        constructor(){
            super();
            this.HasGravity = true;
        }

        public Width = 25;
        public Height = 25;
        public Radius = Math.sqrt(this.Width * this.Width + this.Height * this.Height);
    }
}
