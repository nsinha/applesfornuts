/**
 * Created by Nick on 12/15/2015.
 */
module TheGame
{
    export class Level
    {
        public LevelNumber: number;
        public TargetNuts: number;
        public _currentNuts: number;
        private _apples: number;
        public Chance: number;
        InitializeLevel(levelNumber: number){
            this.LevelNumber = levelNumber;
            var chance: number = (levelNumber - 1);
            this.Chance = Math.min(chance / 25, .8);
            this.TargetNuts = 5;
            this._apples = Math.max(21 - levelNumber, 5);
            this._currentNuts = 0;
        }

        UseApple() : boolean{
            --this._apples;
            if(this._apples == 0){
                return false;
            }
            return true;
        }

        GetApplesLeft() : number {
            return this._apples;
        }

        GotGoodNut(){
            ++this._currentNuts;
        }

        GotBadNut(){
            --this._currentNuts;
            if(this._currentNuts < 0)
            {
                this._currentNuts = 0;
            }
        }

        GetCurrentNutCount(): number
        {
            return this._currentNuts;
        }

    }
}