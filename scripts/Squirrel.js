/// <reference path='GameObject.ts'/>
/**
 * Created by Nikhil on 11/30/2015.
 */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TheGame;
(function (TheGame) {
    var Squirrel = (function (_super) {
        __extends(Squirrel, _super);
        function Squirrel() {
            _super.apply(this, arguments);
            this.Width = 50;
            this.Height = 31;
            this.Radius = Math.sqrt(this.Width * this.Width + this.Height * this.Height);
        }
        return Squirrel;
    })(TheGame.GameObject);
    TheGame.Squirrel = Squirrel;
})(TheGame || (TheGame = {}));
//# sourceMappingURL=Squirrel.js.map