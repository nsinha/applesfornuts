/**
 * Created by Nikhil on 11/30/2015.
 */
/// <reference path='GameRenderer.ts'/>
/// <reference path='GameModel.ts'/>
/// <reference path='SoundManager.ts'/>
var TheGame;
(function (TheGame) {
    var GameRunner = (function () {
        function GameRunner(canvasId) {
            var canvas = document.getElementById(canvasId);
            var that = this;
            var dragMove = function (event) {
                var pos = getCursorPosition(event, canvas);
                that._model.MouseMove(new TheGame.Vector2N(pos.x, pos.y));
            };
            var dragEnd = function (event) {
                var pos = getCursorPosition(event, canvas);
                that._model.MouseUp(new TheGame.Vector2N(pos.x, pos.y));
                canvas.removeEventListener("mousemove", dragMove, true);
                canvas.removeEventListener("mouseup", dragEnd, true);
            };
            canvas.addEventListener('mousedown', function (event) {
                var pos = getCursorPosition(event, canvas);
                if (that._model.MouseDown(new TheGame.Vector2N(pos.x, pos.y))) {
                    canvas.addEventListener("mousemove", dragMove, true);
                    canvas.addEventListener("mouseup", dragEnd, true);
                }
            });
            var ctx = canvas.getContext('2d');
            this._model = new TheGame.GameModel();
            this._renderer = new TheGame.GameRenderer(canvas, ctx, this._model);
            this._sounds = new TheGame.SoundManager();
        }
        GameRunner.prototype.Step = function () {
            this._model.Step(0.017);
        };
        GameRunner.prototype.Draw = function () {
            this._renderer.Draw();
            var sounds = this._model.SoundsToPlay;
            for (var i = 0; i < sounds.length; ++i) {
                this._sounds.Play(sounds[i]);
            }
            this._model.SoundsToPlay = [];
        };
        GameRunner.prototype.Setup = function () {
        };
        return GameRunner;
    })();
    var runner = new GameRunner('gameCanvas');
    runner.Setup();
    setInterval(function () {
        runner.Draw();
        runner.Step();
    }, 17);
})(TheGame || (TheGame = {}));
function getCursorPosition(e, canvas) {
    var x;
    var y;
    if (e.pageX || e.pageY) {
        x = e.pageX;
        y = e.pageY;
    }
    else {
        x = e.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        y = e.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }
    x -= canvas.offsetLeft;
    y -= canvas.offsetTop;
    return { x: x, y: y };
}
//# sourceMappingURL=GameRunner.js.map