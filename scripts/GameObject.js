/**
 * Created by Nikhil on 11/30/2015.
 */
var TheGame;
(function (TheGame) {
    var Vector2N = (function () {
        function Vector2N(x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            this.X = x;
            this.Y = y;
        }
        Vector2N.prototype.DistanceFrom = function (other) {
            var x2 = this.X - other.X;
            x2 = (x2 * x2);
            var y2 = this.Y - other.Y;
            y2 = (y2 * y2);
            return Math.sqrt(x2 + y2);
        };
        Vector2N.prototype.Diff = function (other) {
            var x = this.X - other.X;
            var y = this.Y - other.Y;
            return new Vector2N(x, y);
        };
        Vector2N.prototype.Add = function (other) {
            var x = this.X + other.X;
            var y = this.Y + other.Y;
            return new Vector2N(x, y);
        };
        Vector2N.prototype.Average = function (other) {
            var x = this.X + other.X;
            var y = this.Y + other.Y;
            return new Vector2N(x / 2, y / 2);
        };
        Vector2N.prototype.Size = function () {
            var x2 = this.X;
            x2 = (x2 * x2);
            var y2 = this.Y;
            y2 = (y2 * y2);
            return Math.sqrt(x2 + y2);
        };
        Vector2N.prototype.Scale = function (scale) {
            var x = this.X * scale;
            var y = this.Y * scale;
            return new Vector2N(x, y);
        };
        Vector2N.prototype.Clone = function () {
            return new Vector2N(this.X, this.Y);
        };
        return Vector2N;
    })();
    TheGame.Vector2N = Vector2N;
    var GameObject = (function () {
        function GameObject() {
            this.CurrentPosition = new TheGame.Vector2N();
            this.Velocity = new TheGame.Vector2N();
        }
        GameObject.prototype.ApplyImpulse = function (impulse) {
            this.Velocity = impulse;
        };
        GameObject.prototype.Step = function (timeDelta) {
            if (this.HasGravity) {
                this.Velocity.Y = this.Velocity.Y + (400 * timeDelta);
            }
            this.CurrentPosition.X = this.CurrentPosition.X + (this.Velocity.X * timeDelta);
            this.CurrentPosition.Y = this.CurrentPosition.Y + (this.Velocity.Y * timeDelta);
        };
        return GameObject;
    })();
    TheGame.GameObject = GameObject;
})(TheGame || (TheGame = {}));
//# sourceMappingURL=GameObject.js.map