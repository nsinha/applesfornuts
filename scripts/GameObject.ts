/**
 * Created by Nikhil on 11/30/2015.
 */

module TheGame
{
    export class Vector2N
    {
        constructor(x: number =0, y: number = 0){
            this.X = x;
            this.Y = y;
        }

        DistanceFrom(other: Vector2N): number{
            var x2 = this.X - other.X;
            x2 = (x2 * x2);
            var y2 = this.Y - other.Y;
            y2 = (y2 * y2);
            return Math.sqrt(x2 + y2);
        }

        Diff(other: Vector2N): Vector2N{
            var x = this.X - other.X;
            var y = this.Y - other.Y;
            return new Vector2N(x, y);
        }

        Add(other: Vector2N): Vector2N{
            var x = this.X + other.X;
            var y = this.Y + other.Y;
            return new Vector2N(x, y);
        }

        Average(other: Vector2N): Vector2N{
            var x = this.X + other.X;
            var y = this.Y + other.Y;
            return new Vector2N(x/2, y/2);
        }

        Size(): number{
            var x2 = this.X;
            x2 = (x2 * x2);
            var y2 = this.Y;
            y2 = (y2 * y2);
            return Math.sqrt(x2 + y2);
        }

        Scale(scale: number): Vector2N {
            var x = this.X * scale;
            var y = this.Y * scale;
            return new Vector2N(x, y);
        }

        Clone(): Vector2N{
            return new Vector2N(this.X, this.Y);
        }

        public X: number;
        public Y: number;
    }

    export class GameObject
    {
        public CurrentPosition : Vector2N;
        public Velocity: Vector2N;
        public HasGravity: boolean;

        constructor(){
            this.CurrentPosition = new TheGame.Vector2N();
            this.Velocity = new TheGame.Vector2N();
        }

        ApplyImpulse(impulse: Vector2N): void{
            this.Velocity = impulse;
        }

        Step(timeDelta) : void{
            if(this.HasGravity)
            {
                this.Velocity.Y = this.Velocity.Y + (400 * timeDelta);
            }
            this.CurrentPosition.X = this.CurrentPosition.X + (this.Velocity.X * timeDelta);
            this.CurrentPosition.Y = this.CurrentPosition.Y + (this.Velocity.Y * timeDelta);
        }

    }
}
