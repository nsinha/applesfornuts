/// <reference path='GameObject.ts'/>
/**
 * Created by Nick on 12/15/2015.
 */

module TheGame
{
    export class Nut extends GameObject
    {
        public Width = 20;
        public Height = 20;
        public Poison: boolean;
    }
}