/**
 * Created by Nikhil on 12/3/2015.
 */
module TheGame{
    export class SoundManager{
        private _shoot = new Audio('sounds/Laser_Shoot19.wav');
        private _hit = new Audio('sounds/Hit_Hurt16.wav');
        Play(name: string){
            switch(name){
                case 'shoot':
                    this._shoot.play();
                    break;
                case 'hit':
                    this._hit.play();
                    break;
            }
        }
    }
}